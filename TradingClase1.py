from datetime import datetime
import MetaTrader5 as mt5
# import the 'pandas' module for displaying data obtained in the tabular form
import pandas as pd
pd.set_option('display.max_columns', 500) # number of columns to be displayed
pd.set_option('display.width', 1500)      # max table width to display
# import pytz module for working with time zone
import pytz

account = 68087864
password = 'D1234567890'
server = 'RoboForex-Pro'
path = r'C:\Program Files\MetaTrader 5\terminal64.exe'

authorized = mt5.initialize(login=account, password=password, server=server, path=path)
# if authorized:
#     # display trading account data 'as is'
#     print(mt5.account_info())
#     # display trading account data in the form of a list
#     print("Show account_info()._asdict():")
#     account_info_dict = mt5.account_info()._asdict()
#     for prop in account_info_dict:
#         print("  {}={}".format(prop, account_info_dict[prop]))
# else:
#     print("failed to connect at account #{}, error code: {}".format(account, mt5.last_error()))

# set time zone to UTC
timezone = pytz.timezone("Etc/UTC")
# create 'datetime' object in UTC time zone to avoid the implementation of a local time zone offset
utc_from = datetime(2020, 1, 10, tzinfo=timezone)
# get 10 EURUSD H4 bars starting from 01.10.2020 in UTC time zone
rates = mt5.copy_rates_from_pos("GBPUSD", mt5.TIMEFRAME_M1, 0, 2000)
# display each element of obtained data in a new line
print("Display obtained data 'as is'")
for rate in rates:
    print(rate)

# create DataFrame out of the obtained data
rates_frame = pd.DataFrame(rates)
# convert time in seconds into the datetime format
rates_frame['time']=pd.to_datetime(rates_frame['time'], unit='s')

# display data
print("\nDisplay dataframe with data")
print(rates_frame)


# Crear clases
class Carros():

    def __init__(self, cilindraje, peso, color, marca):
        self.cilindraje = cilindraje
        self.peso = peso
        self.color = color
        self.marca = marca

    def encender_auto(self):
        print(f'el auto de marca {self.marca} esta encendido')

cilindraje = 2000
peso = 40
color = 'rojo'
marca = 'General Motors'

el_carro_de_dany = Carros(cilindraje, peso, color, marca)
el_carro_de_dany.encender_auto()


orden = {
    "action": mt5.TRADE_ACTION_DEAL,
    "symbol": 'EURUSD',
    "volume": 0.01,
    "type": mt5.ORDER_TYPE_SELL,
    "magic": 202204,
    "comment": "My Bot",
    "type_time": mt5.ORDER_TIME_GTC,
    "type_filling": mt5.ORDER_FILLING_FOK
}

value = False

if value:
    for i in range(50):
        mt5.order_send(orden)


    array_pos = mt5.positions_get()
    post = pd.DataFrame(array_pos, columns=array_pos[0]._asdict().keys())
    lista_tickets = post['ticket'].tolist()

    for ticket in lista_tickets:
        # crear un array temporal con la informacion separada de cada operacion abierta
        temporal = post[post['ticket'] == ticket]
        # Obtener el id de todas las operaciones abierta
        deal_id = temporal['ticket'].item()
        # Obtener el lotaje de las operaciones abiertas
        lotaje = temporal['volume'].item()
        
        orden_cierre = {
            "action": mt5.TRADE_ACTION_DEAL,
            "symbol": 'EURUSD',
            "volume": float(lotaje),
            "position": deal_id,
            "type": mt5.ORDER_TYPE_BUY,
            "magic": 202204,
            "comment": "Cierre operacion",
            "type_time": mt5.ORDER_TIME_GTC,
            "type_filling": mt5.ORDER_FILLING_FOK
        }
        mt5.order_send(orden_cierre)
