import pywhatkit
import pandas as pd
import numpy as np
import MetaTrader5 as mt5

account = 68087864
password = 'D1234567890'
server = 'RoboForex-Pro'
path = r'C:\Program Files\MetaTrader 5\terminal64.exe'

def extraer_datos(par, periodo, cantidad):
  '''
  Funcion para extraer los datos de mt5

  # Parametros
  @par: activo a extraer
  @periodo: M1, M5, M15, etc
  @cantidad: entero con el numero de registros a extraer
  '''

  mt5.initialize(login=account, password=password, server=server, path=path)
  rates = mt5.copy_rates_from_pos(par, periodo, 0, cantidad)
  tabla = pd.DataFrame(rates)
  tabla['time']=pd.to_datetime(tabla['time'], unit='s')

  return tabla

df = extraer_datos('EURUSD', mt5.TIMEFRAME_M1, 1000)

def calcular_media_movil(tabla, N):
  '''
  
  '''
  tabla['media_movil'] = tabla['close'].rolling(N).mean()

  return tabla


df1 = calcular_media_movil(df, 20)

# def cierre_pos_abierta():
#   array_pos = mt5.positions_get()
#   post = pd.DataFrame(array_pos, columns=array_pos[0]._asdict().keys())
#   lista_tickets = post['ticket'].tolist()

#   for ticket in lista_tickets:
#       # crear un array temporal con la informacion separada de cada operacion abierta
#       temporal = post[post['ticket'] == ticket]
#       # Obtener el id de todas las operaciones abierta
#       deal_id = temporal['ticket'].item()
#       # Obtener el lotaje de las operaciones abiertas
#       lotaje = temporal['volume'].item()
      
#       orden_cierre = {
#           "action": mt5.TRADE_ACTION_DEAL,
#           "symbol": 'EURUSD',
#           "volume": float(lotaje),
#           "position": deal_id,
#           "type": mt5.ORDER_TYPE_BUY,
#           "magic": 202204,
#           "comment": "Cierre operacion",
#           "type_time": mt5.ORDER_TIME_GTC,
#           "type_filling": mt5.ORDER_FILLING_FOK
#       }
#       mt5.order_send(orden_cierre)

def abrir_operacion(par, volumen, tipo_operacion):
  orden = {
    "action": mt5.TRADE_ACTION_DEAL,
    "symbol": par,
    "volume": volumen,
    "type": tipo_operacion,
    "magic": 202204,
    "comment": "My Bot",
    "type_time": mt5.ORDER_TIME_GTC,
    "type_filling": mt5.ORDER_FILLING_FOK
  }
  print(f'{mt5.TRADE_ACTION_DEAL} {type}')
  mt5.order_send(orden)
  lista = ['+17862957813', '++573218023314', '+573233254820']
  for numero in lista:
    pywhatkit.sendwhatmsg_instantly(numero, f'Hola tio esta es una prueba desde python Se ejecuto una {tipo_operacion} con un volumen de {volumen} en el {par}')
  print(f'Se ejecuto una {tipo_operacion} con un volumen de {volumen} en el {par}')

def robot_udr(df1, volumen, par):
  '''
  '''
  ultimo_ma = df1['media_movil'].iloc[-1]
  penultimo_ma = df1['media_movil'].iloc[-2]

  if ultimo_ma > penultimo_ma:

    abrir_operacion(par, volumen, tipo_operacion=mt5.ORDER_TYPE_BUY)
  
  elif ultimo_ma < penultimo_ma:

    abrir_operacion(par, volumen, tipo_operacion=mt5.ORDER_TYPE_SELL)

robot_udr(df1, 0.01,'EURUSD')
