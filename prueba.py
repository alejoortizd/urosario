import pandas as pd
import MetaTrader5 as mt5

name = 68087864
password = 'D1234567890'
server = 'RoboForex-Pro'
path = r'C:\Program Files\MetaTrader 5\terminal64.exe'
div= 'EURUSD'
per= mt5.TIMEFRAME_M1
can= 1000
vol = 0.02

mt5.initialize(login=name, password=password, server=server, path=path)

def extraer_datos(par, periodo, cantidad):
  posiciones = mt5.copy_rates_from_pos(par, periodo, 0, cantidad)
  data = pd.DataFrame(posiciones)
  data['time'] = pd.to_datetime(data['time'], unit='s')
  data['media_movil_fast'] = data['close'].rolling(50).mean()
  data['media_movil_slow'] = data['close'].rolling(200).mean()
  data = data.set_index('time')

  return data

tipo_operacion = mt5.ORDER_TYPE_SELL
def abrir_operacion(par,volumen,tipo_operacion):
  # parameters
  point = mt5.symbol_info(par).point
  price = mt5.symbol_info_tick(par).ask

  orden = {
  "action": mt5.TRADE_ACTION_DEAL,
  "symbol": par,
  "volume": volumen,
  "type": tipo_operacion,
  "price": price,
  "sl": price - 100 * point,
  "tp": price + 100 * point,
  "magic": 202204,
  "comment": "Bot UdeR1",
  "type_time": mt5.ORDER_TIME_GTC,
  "type_filling": mt5.ORDER_FILLING_FOK

  }

  mt5.order_send(orden)
  compra = 'Venta' if tipo_operacion == 1 else 'Compra'
  print(f'Se hizo {compra} en el {par} con precio: {price} con lotaje de: {volumen}')

abrir_operacion(div, vol, tipo_operacion)

# def robot_medias_Moviles(data, vol, par):
#   print(data)
#   tendencia = data['media_movil_fast'].iloc[-1] - data['media_movil_slow'].iloc[-1]
#   print(tendencia)

#   tipo_operacion = mt5.ORDER_TYPE_BUY if tendencia > 0 else mt5.ORDER_TYPE_SELL
#   compra = 'Venta' if tipo_operacion == 1 else 'Compra'
#   print(compra)
#   if tendencia > 0:
#     abrir_operacion(par, vol, tipo_operacion)
#   else:
#     abrir_operacion(par, vol, tipo_operacion)





# data = extraer_datos(div, per, can)
# robot_medias_Moviles(data, vol, div)


