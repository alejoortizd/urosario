import pywhatkit
import pandas as pd
import MetaTrader5 as mt5

name = 68087864
password = 'D1234567890'
server = 'RoboForex-Pro'
path = r'C:\Program Files\MetaTrader 5\terminal64.exe'

class Robot():

  def __init__(self, name, password, server, path):
    """
    Funcion que inicializa el robot de trading
    ------------
    # Parametros
    name: int
      numero de la cuenta
    password: str
      password asociado al name
    server: str
      servidor donde se encuentra el name
    path: str
      lugar donde se instalo metatrader5
    """
    self.name = name
    self.password = password
    self.server = server
    self.path = path

  def extraer_datos(self, par, periodo, cantidad):
    mt5.initialize(login=name, password=password, server=server, path=path)
    posiciones = mt5.copy_rates_from_pos(par, periodo, 0, cantidad)
    data = pd.DataFrame(posiciones)
    data['time'] = pd.to_datetime(data['time'], unit='s')

    return data

  def calcular_media_movil(self, data, smaf, smas):
    data['media_movil_fast'] = data['close'].rolling(smaf).mean()
    data['media_movil_slow'] = data['close'].rolling(smas).mean()

    return data

  def abrir_operacion(self,par,volumen,tipo_operacion):
    # parameters
    point = mt5.symbol_info(par).point
    price = mt5.symbol_info_tick(par).ask

    orden = {
      "action": mt5.TRADE_ACTION_DEAL,
      "symbol": par,
      "volume": volumen,
      "type": tipo_operacion,
      "price": price,
      "sl": price - 100 * point,
      "tp": price + 100 * point,
      "magic": 202204,
      "comment": "Bot UdeR1",
      "type_time": mt5.ORDER_TIME_GTC,
      "type_filling": mt5.ORDER_FILLING_FOK
    }

    mt5.order_send(orden)

    compra = 'Venta' if tipo_operacion == 1 else 'Compra'
    pywhatkit.sendwhatmsg_instantly('+573233254820', f'Hola Se ejecuto {compra} en el {par} con precio: {price} con lotaje de: {volumen} el take profit es {price + 100 * point} y el stop lost es {price - 100 * point}')
    print(f'Se ejecuto {compra} en el {par} con precio: {price} con lotaje de: {volumen}')

  def robot_medias_Moviles(self, data, vol, par):
    tendencia = data['media_movil_fast'].iloc[-1] - data['media_movil_slow'].iloc[-1]

    tipo_operacion = mt5.ORDER_TYPE_BUY if tendencia > 0 else mt5.ORDER_TYPE_SELL
    compra = 'Venta' if tipo_operacion == 1 else 'Compra'
    print(compra)
    if tendencia > 0:
      self.abrir_operacion(par, vol, tipo_operacion)
    else:
      self.abrir_operacion(par, vol, tipo_operacion)

  def handler_robot(self, par, periodo, cantidad, smaf, smas, volumen):
    data = self.extraer_datos(par, periodo, cantidad)
    new_data = self.calcular_media_movil(data, smaf, smas)
    self.robot_medias_Moviles(new_data, volumen, par)

ur = Robot(name, password, server, path)
ur.handler_robot('EURUSD', mt5.TIMEFRAME_M1, 1000, 50, 200, 0.02)
